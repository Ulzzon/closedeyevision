﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using ClosedEyedVisualsApp;
using tessnet2;
using System.IO;


namespace ClosedEyedVisualsGUI
{
    public partial class Form1 : Form
    {
        private Int32 timeStampStart;
        private int timeStampEnd;
        private int totalProcessingTime;
        private bool DeviceExist = false;
        private FilterInfoCollection videoDevices;
        private VideoCaptureDevice videoSource = null;
        private bool videoSourceInitialized;
        private int videoProperty_ResolutionId = 2; //0 = 720p, 1= 800x600, 2=640x480
        private enum videoResolution {r_720p = 0, r_800x600 = 1, r_640x480 = 2 };
        private Bitmap CameraImage;
        ErrorProvider resultHelper = new ErrorProvider();
        //public InputDataHandler templateInputData = new InputDataHandler();

        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox_videoResolution.DataSource = EnumUtil.GetValues<videoResolution>();
            getCamList();
        }
        
        // get the devices name
        private void getCamList()
        {
            try
            {
                videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                comboBox_VideoDevicesList.Items.Clear();
                if (videoDevices.Count == 0)
                    throw new ApplicationException();

                DeviceExist = true;
                foreach (FilterInfo device in videoDevices)
                {
                    comboBox_VideoDevicesList.Items.Add(device.Name);
                }
            }
            catch (ApplicationException)
            {
                DeviceExist = false;
                comboBox_VideoDevicesList.Items.Add("No capture device on your system");
            }
            comboBox_VideoDevicesList.SelectedIndex = 0; //make dafault to first cam
            comboBox_VideoDevicesList.Refresh();
        }

        //refresh button
        private void button_RefreshVideoDeviceList_Click(object sender, EventArgs e)
        {
            getCamList();
        }

        //Start Capturing from Camera
        private void button_StartImageSource_Click(object sender, EventArgs e)
        {
            if (button_StartImageSource.Text == "Start")
            {                
                if (DeviceExist)
                {
                   
                    videoSource = new VideoCaptureDevice(videoDevices[comboBox_VideoDevicesList.SelectedIndex].MonikerString);
                    videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);
                    CloseVideoSource();

                    videoProperty_ResolutionId = Int32.Parse(textBox_VideoMode.Text);  // Read VideoMode from form
                    if (videoProperty_ResolutionId >= videoSource.VideoCapabilities.Length)
                    {
                        videoProperty_ResolutionId = 0;
                        MessageBox.Show("VideoMode not available, using default");
                    }
                    videoSource.VideoResolution = videoSource.VideoCapabilities[videoProperty_ResolutionId];
                    textBox_CameraInfo.AppendText("VideoResolution:" + videoSource.VideoCapabilities[videoProperty_ResolutionId].FrameSize.ToString());
                    textBox_CameraInfo.AppendText("VideoFrameRate:" + videoSource.VideoCapabilities[videoProperty_ResolutionId].AverageFrameRate.ToString());
                    textBox_CameraInfo.Refresh();
                    
                    videoSource.Start();
                    label_ImageSourceStatus.Text = "Device running...";
                    button_StartImageSource.Text = "Stop";
                    timer_Runtime.Enabled = true;
                    videoSourceInitialized = true;
                }
                else
                {
                    label_ImageSourceStatus.Text = "Error: No Device selected.";
                    label_ImageSourceStatus.Refresh();
                }
            }
            else
            {
                //if (videoSource.IsRunning)
                if (videoSourceInitialized)
                {
                    timer_Runtime.Enabled = false;
                    CloseVideoSource();
                    label_ImageSourceStatus.Text = "Device stopped.";
                    button_StartImageSource.Text = "Start";
                }
            }
        }

        //eventhandler if new frame is ready
        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            CameraImage = (Bitmap)eventArgs.Frame.Clone();
            //do processing here
            pictureBox_ImageSource.Image = CameraImage;

        }

        //close the device safely
        private void CloseVideoSource()
        {
            if (!(videoSource == null))
                if (videoSource.IsRunning)
                {
                    videoSource.SignalToStop();
                    videoSource = null;
                    videoSourceInitialized = false;
                }
        }

        //prevent sudden close while device is running
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseVideoSource();
        }


        private InputDataHandler GetTempateDataFromForm()
        {
            InputDataHandler templateInputData = new InputDataHandler();
            try
            {
                if (videoSourceInitialized)
                {
                    templateInputData.SourceImage = CameraImage;
                    textBox_CameraInfo.AppendText("ImageSource Webcam..");
                    textBox_CameraInfo.Refresh();
                }
                else {
                    //Program.sourceImageAddress = textBox_SourceImage.Text;
                    templateInputData.SourceImage = (Bitmap)Bitmap.FromFile(textBox_SourceImage.Text);
                    label_ResultOutput.Text = "ImageSource src.jpg..";
                    label_ResultOutput.Refresh();
                }

                templateInputData.TemplateImage = (Bitmap)Bitmap.FromFile(textBox_TemplateImage.Text);
                templateInputData.PyramidLevels = Int32.Parse(textBox_PyramidLevels.Text);
                templateInputData.Threshold = float.Parse(textBox_Treshold.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("Error in setting formData");
            }
            return templateInputData;
        }

        private InputDataHandler AddRoiFromGUI(InputDataHandler templateInputData)
        {
            try
            {
                templateInputData.RoiWidth = Int32.Parse(textBox_RoiWidth.Text);
                templateInputData.RoiHeight = Int32.Parse(textBox_RoiHeight.Text);
                templateInputData.RoiPointX = Int32.Parse(textBox_PointX.Text);
                templateInputData.RoiPointY = Int32.Parse(textBox_PointY.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show("Error in setting formData");
            }
            return templateInputData;
        }
        

        private void button_SourceImage_openFile_Click(object sender, EventArgs e)
        {
            string filePath = ChooseImageDialog();
            if (filePath != String.Empty)
            {
                textBox_SourceImage.Text = filePath;
                pictureBox_ImageSource.Image = Image.FromFile(filePath);
            }
        }

        private void button_TemplateImage_openFile_Click(object sender, EventArgs e)
        {
            textBox_TemplateImage.Text = ChooseImageDialog();;
        }

        //Matching Functions
        private void button_ExhaustiveMatchingInROI_Click(object sender, EventArgs e)
        {
            var templateInputData = GetTempateDataFromForm();
            templateInputData = AddRoiFromGUI(templateInputData);
            label_ResultOutput.Text = "Processing ....";
            label_ResultOutput.Refresh();
            timeStampStart = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            TemplateDetection templateDetectionObj = new TemplateDetection(templateInputData);
            templateDetectionObj.FindTemplateInROI(TemplateDetectionResult_Event);
        }

        private void button_ExhaustiveMatching_Click(object sender, EventArgs e)
        {

            var templateInputData = GetTempateDataFromForm();

            label_ResultOutput.Text = "Processing ....";
            label_ResultOutput.Refresh();
            timeStampStart = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            TemplateDetection templateDetectionObj = new TemplateDetection(templateInputData);
            templateDetectionObj.FindTemplateInImageExhaustively(TemplateDetectionResult_Event);
        }

        public void TemplateDetectionResult_Event(TemplateMatchingResults result)
        {
            if (result.MatchFound)
            {
                label_ResultOutput.Text = "Match Found";
                resultHelper.Clear();
            }
            else if(!result.MatchFound && result.ResultException != null)
            {
                label_ResultOutput.Text = "Somthing went wrong";
                resultHelper.SetError(label_ResultOutput, result.ResultException);
            }
            else
            {
                label_ResultOutput.Text = "Match Not found";
                resultHelper.Clear();
            }

            label_ResultOutput.Refresh();

            timeStampEnd = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            totalProcessingTime = timeStampEnd - timeStampStart;
            label_ProcessingTimeOutput.Text = totalProcessingTime.ToString() + " s";
            label_ProcessingTimeOutput.Refresh();

            label_SimilarityOutput.Text = result.SimilarityMax.ToString();
            label_SimilarityOutput.Refresh();
        }

        private void timer_Runtime_Tick(object sender, EventArgs e)
        {
            label_ImageSourceStatus.Text = "Device running... " + videoSource.FramesReceived.ToString() + " FPS";
            label_ImageSourceStatus.Refresh();
        }

        private void button_TakeSnapshot_Click(object sender, EventArgs e)
        {
            if (videoSourceInitialized)
            {
                if (pictureBox_ImageSource.Image != null)
                {
                    Bitmap varBmp = new Bitmap(pictureBox_ImageSource.Image);
                    varBmp.Save("SnapShot.bmp", ImageFormat.Bmp);
                    varBmp.Dispose();
                    varBmp = null;
                }
                else {
                    MessageBox.Show("Null Exception");
                }
            }
            else {
                textBox_CameraInfo.AppendText("VideoSource not initialized");
                textBox_CameraInfo.Refresh();
            }
        }

        private void button_StartOCR_Click(object sender, EventArgs e)
        {
            Bitmap image;
            if (videoSourceInitialized)
            {
                image = CameraImage;
            }
            else { 
                image = (Bitmap)Bitmap.FromFile(textBox_OcrImage.Text);
            }
            progressBar_Ocr.Value = 0;
            var ocrObj = new ClosedEyeVisualsOCR();
            ocrObj.setProgressListner(OcrProgress);
            ocrObj.GetTextFromImage(image, OcrResult_Event);
        }

        delegate void OcrResult_Callback(OcrResult text);
        public void OcrResult_Event(OcrResult result)
        {
            if (textBox_OcrResult.InvokeRequired)
            {
                OcrResult_Callback d = new OcrResult_Callback(OcrResult_Event);
                this.Invoke(d, new object[] { result });
            }
            else
            {
                foreach (Word word in result.GetFoundWords())
                {
                    Console.WriteLine("{0} : {1}", word.Confidence, word.Text);
                    textBox_OcrResult.AppendText("Confidence:" + word.Confidence.ToString() + ", Text: " + word.Text + "\n");
                    textBox_OcrResult.Refresh();
                }
            }
        }

        private void button_OCR_openFile_Click(object sender, EventArgs e)
        {
            string fileNameWithPath = ChooseImageDialog();
            if (fileNameWithPath != String.Empty)
            {
                textBox_OcrImage.Text = fileNameWithPath;
                if (!videoSourceInitialized)
                {
                    Image image = Image.FromFile(fileNameWithPath);
                    pictureBox_ImageSource.Image = image;
                    ImageView imageView = new ImageView(image);
                    imageView.Show();
                }
            }
        }

        private string ChooseImageDialog()
        {
            OpenFileDialog ocr_fielDialog = new OpenFileDialog();

            ocr_fielDialog.Title = "Select Image";
            ocr_fielDialog.Filter = "Image Files (*.jpg,*.png)|*.jpg; *.png|All Files (*.*)|*.*";
            ocr_fielDialog.FilterIndex = 1;
            var fileNameWithPath = String.Empty;
            if (ocr_fielDialog.ShowDialog() == DialogResult.OK)
            {
                fileNameWithPath = Path.GetFullPath(ocr_fielDialog.FileName).ToString();
            }
            return fileNameWithPath;
        }

        delegate void OcrProgress_Callback(int percent);
        private void OcrProgress(int percent)
        {
            if (progressBar_Ocr.InvokeRequired)
            {
                OcrProgress_Callback d = new OcrProgress_Callback(OcrProgress);
                this.Invoke(d, new object[] { percent });
            }
            else
            {
                progressBar_Ocr.Value = percent;
            }
        }
    }
}
