﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClosedEyedVisualsApp;

namespace ClosedEyedVisualsGUI
{
    public partial class ImageView : Form
    {
        private Image originalImage;
        private Point paintStartPoint;
        private Point paintStopPoint;
        protected Pen roiBrush = new Pen(Color.Blue, 2f);
        protected Pen orientationLineBrush = new Pen(Color.Red, 2f);

        public ImageView(Image image)
        {
            InitializeComponent();
            originalImage = image;
            loadOriginalImage();
        }

        private void loadOriginalImage()
        {
            pictureBox_MainImage.Image = (Image)originalImage.Clone();
            updateToolStripStatus();
            centerLineToolStripMenuItem.Checked = false;
        }

        private void updateToolStripStatus()
        {
            toolStripStatusLabel_Size.Text = String.Format("Size: ({0}x{1})", pictureBox_MainImage.Image.Width, pictureBox_MainImage.Image.Height); 
        }

        private void centerLineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (((ToolStripMenuItem)sender).Checked)
            {
                Image activeImage = pictureBox_MainImage.Image;
                Graphics centerLineImage = Graphics.FromImage(activeImage);
                Point leftPoint = new Point(0, activeImage.Height / 2);
                Point rightPoint = new Point(activeImage.Width, activeImage.Height / 2);
                Point topPoint = new Point(activeImage.Width / 2, activeImage.Height);
                Point buttomPoint = new Point(activeImage.Width / 2, 0);
                centerLineImage.DrawLine(orientationLineBrush, leftPoint, rightPoint);
                centerLineImage.DrawLine(orientationLineBrush, topPoint, buttomPoint);
                pictureBox_MainImage.Refresh();
            }
            else
            {
                loadOriginalImage();
            }
        }

        private void originalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadOriginalImage();
        }

        private void gridToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (((ToolStripMenuItem)sender).Checked)
            {
                drawVerticalGrid();
                drawHorisontalGrid();
            }
            else
            {
                loadOriginalImage();
            }
        }

        private void drawVerticalGrid()
        {
            Image activeImage = pictureBox_MainImage.Image;
            Graphics centerLineImage = Graphics.FromImage(activeImage);
            int imageHeight = activeImage.Height;
            int imageWidth = activeImage.Width;
            float verticalSpace = imageWidth/10;
            Point topPoint = new Point(0,0);
            Point bottomPoint = new Point(0,imageHeight);
            for(int i = 1; i < 10; i++)
            {
                topPoint.X = (int)(i*verticalSpace);
                bottomPoint.X = (int)(i*verticalSpace);
                centerLineImage.DrawLine(orientationLineBrush, topPoint, bottomPoint);
            }
            pictureBox_MainImage.Refresh();
        }

        private void drawHorisontalGrid()
        {
            Image activeImage = pictureBox_MainImage.Image;
            Graphics centerLineImage = Graphics.FromImage(activeImage);
            int imageHeight = activeImage.Height;
            int imageWidth = activeImage.Width;
            float horisontalSpace = imageHeight / 10;
            Point leftPoint = new Point(0, 0);
            Point rightPoint = new Point(imageWidth,0);
            for (int i = 1; i < 10; i++)
            {
                leftPoint.Y = (int)(i * horisontalSpace);
                rightPoint.Y = (int)(i * horisontalSpace);
                centerLineImage.DrawLine(orientationLineBrush, leftPoint, rightPoint);
            }
            pictureBox_MainImage.Refresh();
        }

        private void grayScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap grayImage = ImageUtil.MakeGrayscale3((Bitmap)originalImage.Clone());
            pictureBox_MainImage.Image = grayImage;
        }

        private void pictureBox_MainImage_MouseDown(object sender, MouseEventArgs e)
        {
            if (rOIToolStripMenuItem.Checked || workAreaToolStripMenuItem.Checked)
            {
                Point mouseCoordinates = new Point(e.X, e.Y);
                paintStartPoint = calculateCoordinateWithZoomLevel(pictureBox_MainImage.Image, pictureBox_MainImage, mouseCoordinates);
            }
        }

        private void pictureBox_MainImage_MouseUp(object sender, MouseEventArgs e)
        {
            if (rOIToolStripMenuItem.Checked || workAreaToolStripMenuItem.Checked)
            {
                Point mouseCoordinates = new Point(e.X, e.Y);
                paintStopPoint = calculateCoordinateWithZoomLevel(pictureBox_MainImage.Image, pictureBox_MainImage, mouseCoordinates);
                drawRectangle(paintStartPoint, paintStopPoint);
                if (workAreaToolStripMenuItem.Checked)
                {
                    Rectangle cropArea = createRectangleFromPoints(paintStartPoint, paintStopPoint);
                    ImageView workImage = new ImageView(ImageUtil.cropImage(originalImage, cropArea));
                    workImage.Show();
                }
            }
            
        }

        private void drawRectangle(Point startPoint, Point stopPoint)
        {
            Image activeImage = pictureBox_MainImage.Image;
            Graphics graphicImage = Graphics.FromImage(activeImage);
            Rectangle rec = createRectangleFromPoints(startPoint, stopPoint);
            graphicImage.DrawRectangle(roiBrush, rec);
            pictureBox_MainImage.Refresh();
            toolStripStatusLabel_RoI.Text = "RoI: " + rec.Location.ToString();
        }

        private Point calculateCoordinateWithZoomLevel(Image image, PictureBox container, Point picuterBoxPoint)
        {
            float imageRatio = image.Width / (float)image.Height; // image W:H ratio
            float containerRatio = container.Width / (float)container.Height; // container W:H ratio
            Point unscaled_p = new Point();
            if (imageRatio >= containerRatio)
            {
                // horizontal image
                float scaleFactor = container.Width / (float)image.Width;
                float scaledHeight = image.Height * scaleFactor;
                // calculate gap between top of container and top of image
                float filler = Math.Abs(container.Height - scaledHeight) / 2;
                unscaled_p.X = (int)(picuterBoxPoint.X / scaleFactor);
                unscaled_p.Y = (int)((picuterBoxPoint.Y - filler) / scaleFactor);
            }
            else
            {
                // vertical image
                float scaleFactor = container.Height / (float)image.Height;
                float scaledWidth = image.Width * scaleFactor;
                float filler = Math.Abs(container.Width - scaledWidth) / 2;
                unscaled_p.X = (int)((picuterBoxPoint.X - filler) / scaleFactor);
                unscaled_p.Y = (int)(picuterBoxPoint.Y / scaleFactor);
            }

            return unscaled_p;
        }

        private Rectangle createRectangleFromPoints(Point startPoint, Point stopPoint)
        {
            Rectangle rec;
            if (stopPoint.X > startPoint.X && stopPoint.Y > startPoint.Y)
            {
                rec = new Rectangle(startPoint.X, startPoint.Y, stopPoint.X - startPoint.X, stopPoint.Y - startPoint.Y);
            }
            else if (stopPoint.X > startPoint.X && stopPoint.Y < startPoint.Y)
            {
                rec = new Rectangle(startPoint.X, stopPoint.Y, stopPoint.X - startPoint.X, startPoint.Y - stopPoint.Y);
            }
            else if (stopPoint.X < startPoint.X && stopPoint.Y > startPoint.Y)
            {
                rec = new Rectangle(stopPoint.X, startPoint.Y, startPoint.X - stopPoint.X, stopPoint.Y - startPoint.Y);
            }
            else
            {
                rec = new Rectangle(stopPoint.X, stopPoint.Y, startPoint.X - stopPoint.X, startPoint.Y - stopPoint.Y);
            }
            return rec;
        }
    }
}
