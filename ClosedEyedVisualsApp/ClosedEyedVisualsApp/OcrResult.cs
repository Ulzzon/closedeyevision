﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using tessnet2;

namespace ClosedEyedVisualsApp
{
    public class OcrResult
    {
        private List<Word> foundWords = new List<Word>();
        public List<Word> GetFoundWords() { return foundWords; }
        public void Add(Word newWord) 
        {
            this.foundWords.Add(newWord);
        }
    }
}
