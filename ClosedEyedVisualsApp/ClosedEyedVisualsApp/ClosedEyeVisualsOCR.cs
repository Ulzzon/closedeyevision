﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using tessnet2;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;



namespace ClosedEyedVisualsApp
{
    public class ClosedEyeVisualsOCR
    {
        private Bitmap image;
        private Action<OcrResult> resultCallback;
        private Action<int> progress;

        public List<Word> GetTextFromImage( Bitmap image)
        {
            List<Word> tempResult;
            List<Word> result = new List<Word>();
            try
            {
                var ocr = new Tesseract();
                //ocr.SetVariable("tessedit_char_whitelist", "0123456789");
                ocr.SetVariable("tessedit_char_whitelist", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:<>"); 
                ocr.Init("tessdata", "eng", false);
                tempResult = ocr.DoOCR(image, Rectangle.Empty);
                
                // Weed out the bad match results.
                foreach (Word word in tempResult)
                {
                    if (word.Confidence < 160) {
                        result.Add(word);
                    }
                }
            }
            catch (Exception e)
            {
                throw new ClosedEyedVisuals_Exception("OCR error:" + e.ToString());
            }


            return result;
        }

        public void GetTextFromImage(Bitmap image, Action<OcrResult> resultCallback)
        {
            this.image = image;
            this.resultCallback = resultCallback;
            Thread ocrThread = new Thread(new ThreadStart(ExecuteOcrThread));
            ocrThread.Start();
        }

        private void ExecuteOcrThread()
        {
            List<Word> tempResult;
            OcrResult result = new OcrResult();
            try
            {
                var ocr = new Tesseract();
                //ocr.SetVariable("tessedit_char_whitelist", "0123456789");
                ocr.SetVariable("tessedit_char_whitelist", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:<>");
                ocr.Init("tessdata", "eng", false);
                tempResult = ocr.DoOCR(image, Rectangle.Empty);
                ocr.ProgressEvent += OcrProgressEvent;
                // Weed out the bad match results.
                foreach (Word word in tempResult)
                {
                    if (word.Confidence < 160)
                    {
                        result.Add(word);
                    }
                }
            }
            catch (Exception e)
            {
                throw new ClosedEyedVisuals_Exception("OCR error:" + e.ToString());
            }
            resultCallback(result);
        }

        public void setProgressListner(Action<int> progressListner)
        {
            this.progress = progressListner;
        }

        private void OcrProgressEvent(int percent)
        {
            Console.WriteLine("{0}% progression", percent);
            if (this.progress != null)
            {
                progress(percent);
            }
        }
    }
}
